; Drüpen makefile.

; Contrib

projects[admin][subdir] = "contrib"
projects[admin][version] = "2.0"

projects[adminrole][subdir] = "contrib"
projects[adminrole][version] = "1.3"

projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.2"

projects[authoring_html][subdir] = "contrib"
projects[authoring_html][version] = "1.0"

projects[autoload][subdir] = "contrib"
projects[autoload][version] = "2.1"

projects[better_formats][subdir] = "contrib"
projects[better_formats][version] = "1.2"

projects[blocktheme][subdir] = "contrib"
projects[blocktheme][version] = "1.2"

projects[cck][subdir] = "contrib"
projects[cck][version] = "2.9"

projects[cdn][subdir] = "contrib"
projects[cdn][version] = "2.x-dev"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.8"

projects[ckeditor][subdir] = "contrib"
projects[ckeditor][version] = "1.6"
; http://drupal.org/node/932188
projects[ckeditor][patch][] = "http://drupal.org/files/ckeditor_932188_3.patch"
; http://drupal.org/node/1311786
projects[ckeditor][patch][] = "http://drupal.org/files/ckeditor-form_alter.patch"

projects[ckeditor_link][subdir] = "contrib"
projects[ckeditor_link][version] = "2.1"

projects[ckeditor_swf][subdir] = "contrib"
projects[ckeditor_swf][version] = "2.2"

projects[comment_notify][subdir] = "contrib"
projects[comment_notify][version] = "1.6"

projects[content_profile][subdir] = "contrib"
projects[content_profile][version] = "1.0"

projects[context][subdir] = "contrib"
projects[context][version] = "3.0"

projects[date][subdir] = "contrib"
projects[date][version] = "2.7"
; Fixes PHP warning
projects[date][patch][] = "https://gist.github.com/raw/1290346/8abb5e340cdd2a0b97c710d6739b5a98af35f4a2/date_api-php-warning.patch"

projects[date_popup_authored][subdir] = "contrib"
projects[date_popup_authored][version] = "1.1"

projects[diff][subdir] = "contrib"
projects[diff][version] = "2.3"

projects[elysia_cron][subdir] = "contrib"
projects[elysia_cron][version] = "1.2"

projects[features][subdir] = "contrib"
projects[features][version] = "1.1"

projects[filefield][subdir] = "contrib"
projects[filefield][version] = "3.10"
; http://drupal.org/node/984252
projects[filefield][patch][] = "http://drupal.org/files/issues/filefield.transliterate_when_file_created_programatically.patch"

projects[gravatar][subdir] = "contrib"
projects[gravatar][version] = "1.10"

projects[link][subdir] = "contrib"
projects[link][version] = "2.9"

projects[getid3][subdir] = "contrib"
projects[getid3][version] = "1.5"
; http://drupal.org/node/556970
projects[getid3][patch][] = "http://drupal.org/files/issues/getid3_exception_handling.patch"

projects[globalredirect][subdir] = "contrib"
projects[globalredirect][version] = "1.2"

projects[google_analytics][subdir] = "contrib"
projects[google_analytics][version] = "3.3"

projects[htmlpurifier][subdir] = "contrib"
projects[htmlpurifier][version] = "2.4"
; See Authoring HTML README.txt: http://drupalcode.org/project/authoring_html.git/blob/807e51e7fdc78479544e889136e49bd18f822663:/README.txt
projects[htmlpurifier][patch][] = "http://drupalcode.org/project/authoring_html.git/blob_plain/807e51e7fdc78479544e889136e49bd18f822663:/patches/htmlpurifier.patch"

projects[imageapi][subdir] = "contrib"
projects[imageapi][version] = "1.10"

projects[imagecache][subdir] = "contrib"
projects[imagecache][version] = "2.0-beta12"

projects[imagefield][subdir] = "contrib"
projects[imagefield][version] = "3.10"

projects[imagefield_crop][subdir] = "contrib"
projects[imagefield_crop][version] = "1.0"

projects[imce][subdir] = "contrib"
projects[imce][version] = "2.2"

projects[imce_mkdir][subdir] = "contrib"
projects[imce_mkdir][version] = "1.1"

projects[jquery_update][subdir] = "contrib"
projects[jquery_update][version] = "2.0-alpha1"

projects[jquery_ui][subdir] = "contrib"
projects[jquery_ui][version] = "1.5"

projects[libraries][subdir] = "contrib"
projects[libraries][version] = "1.0"

projects[l10n_update][subdir] = "contrib"
projects[l10n_update][version] = "1.0-beta1"
; http://drupal.org/node/1311120
projects[l10n_update][patch][] = "http://drupal.org/files/l10n_update-build_projects_schema_issue.patch"

projects[mollom][subdir] = "contrib"
projects[mollom][version] = "1.16"

projects[nodeformcols][subdir] = "contrib"
projects[nodeformcols][version] = "1.6"

projects[nodereference_url][subdir] = "contrib"
projects[nodereference_url][version] = "1.11"

projects[nodewords][subdir] = "contrib"
projects[nodewords][version] = "2.0-alpha1"

projects[pagepreview][subdir] = "contrib"
projects[pagepreview][version] = "1.x-dev"

projects[pathauto][subdir] = "contrib"
projects[pathauto][version] = "1.5"

projects[path_redirect][subdir] = "contrib"
projects[path_redirect][version] = "1.0-rc2"

projects[private][subdir] = "contrib"
projects[private][version] = "1.1"
; http://drupal.org/node/967766
projects[private][patch][] = "http://drupal.org/files/issues/private.module.vertical_tabs.patch"
; http://drupal.org/node/967762
projects[private][patch][] = "http://drupal.org/files/issues/private.module.act_as_field_0.patch"
; http://drupal.org/node/899860
projects[private][patch][] = "http://drupal.org/files/issues/private-899860.patch"
; http://drupal.org/node/1311128
projects[private][patch][] = "http://drupal.org/files/private-node_grant_rebuild_0.patch"

projects[private_download][subdir] = "contrib"
projects[private_download][version] = "1.3"

projects[scheduler][subdir] = "contrib"
projects[scheduler][version] = "1.8"

projects[search404][subdir] = "contrib"
projects[search404][version] = "1.11"

projects[search_config][subdir] = "contrib"
projects[search_config][version] = "1.6"
; http://drupal.org/node/848588
projects[search_config][patch][] = "http://drupal.org/files/issues/search_config-848588-2.patch"

projects[service_links][subdir] = "contrib"
projects[service_links][version] = "2.1"

projects[seo_checklist][subdir] = "contrib"
projects[seo_checklist][version] = "3.0"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[tagadelic][subdir] = "contrib"
projects[tagadelic][version] = "1.3"

projects[token][subdir] = "contrib"
projects[token][version] = "1.16"

projects[transliteration][subdir] = "contrib"
projects[transliteration][version] = "3.0"

projects[vertical_tabs][subdir] = "contrib"
projects[vertical_tabs][version] = "1.0-rc2"

projects[views][subdir] = "contrib"
projects[views][version] = "2.12"

projects[views_attach][subdir] = "contrib"
projects[views_attach][version] = "2.2"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "1.12"

projects[views_rss][subdir] = "contrib"
projects[views_rss][version] = "1.0-beta5"

projects[webform][subdir] = "contrib"
projects[webform][version] = "3.14"

projects[xmlsitemap][subdir] = "contrib"
projects[xmlsitemap][version] = "2.0-beta3"
; http://drupal.org/node/920062#comment-4288474
projects[xmlsitemap][patch][] = "http://drupal.org/files/issues/xmlsitemap.xmlsitemap.inc_920062.patch"


; Drüpen Features
; Features modules, we just download all them from a single repository.
libraries[drupen][destination] = "modules"
libraries[drupen][download][type] = "git"
libraries[drupen][download][url] = "git://github.com/Infranology/drupen_features.git"
libraries[drupen][download][tag] = "6.x-1.0"



; Libraries


; CKEditor 3.6.2
libraries[ckeditor][download][type] = "get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"

; HTML Purifier 4.3.0
libraries[htmlpurifier][download][type] = "git"
libraries[htmlpurifier][download][url] = "git://repo.or.cz/htmlpurifier.git"
libraries[htmlpurifier][download][tag] = "v4.3.0"

; getID3() 1.9.0
libraries[getid3][download][type] = "get"
libraries[getid3][download][url] = "http://sourceforge.net/projects/getid3/files/getID3%28%29%201.x/1.9.0/getid3-1.9.0-20110620.zip/download"

; jQuery UI 1.6
libraries[jquery.ui][destination] = "modules/contrib/jquery_ui"
libraries[jquery.ui][download][type] = "get"
libraries[jquery.ui][download][url] = "http://jquery-ui.googlecode.com/files/jquery.ui-1.6.zip"

; Wordpress Audio Player (no clear versioning)
libraries[audio-player][destination] = "libraries/players"
libraries[audio-player][download][type] = "get"
libraries[audio-player][download][url] = "http://wpaudioplayer.com/wp-content/downloads/audio-player-standalone.zip"



; Themes

; Zen nineSixty, patched.
projects[zen_ninesixty][version] = "4.3"
; http://drupal.org/node/984796 & http://drupal.org/node/983542
projects[zen_ninesixty][patch][] = "https://gist.github.com/raw/1285773/45a9945fe3f5d135a4f7e41df6efeb1a1b1ef125/aggregated-patches-983542-984796.patch"
; Fixes CDN compatibility
projects[zen_ninesixty][patch][] = "https://gist.github.com/raw/1285773/d59f65054eba104f3bca7e60b43f7d3022f7b5ce/cdn-compat.patch"
; Fixes skip links
projects[zen_ninesixty][patch][] = "https://gist.github.com/raw/1285773/204468b2fbdfe24d46c39308811e4e93c76f76f9/skip-links.patch"
; Remove regions that does not exists
projects[zen_ninesixty][patch][] = "https://gist.github.com/raw/1285773/3f67cc6782dfd1210f7126706c57a088620f22da/unnavailable-regions.patch"

projects[october][type] = "theme"
projects[october][download][type] = "git"
projects[october][download][url] = "git://github.com/Infranology/october.git"
projects[october][download][tag] = "6.x-1.0"

projects[rubik][version] = "3.0-beta2"
projects[tao][version] = "3.2"
projects[zen][version] = "2.1"
