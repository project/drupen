Drupal.behaviors.installProfileTimezone = function(context) {
  if (null === $.cookie('drupen.timezone')) {
    $.cookie('drupen.timezone', Drupal.settings.drupen.timezone);
  }

  if (0 < $('select#edit-date-default-timezone-name').length) {
    $('#edit-date-default-timezone-name').val($.cookie('drupen.timezone'));
  }
}
