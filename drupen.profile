<?php

/**
 * @file
 * Drüpen installation profile.
 *
 * This install profile also provide additional hooks, a way to create
 * "sub-installers":
 *
 * - hook_site_information()
 * - hook_profile_core_modules()
 * - hook_profile_contrib_modules()
 * - hook_profile_extra_modules()
 * - hook_profile_features_modules()
 * - hook_profile_blacklist_modules()
 *
 * The translations import was based in the Localized Drupal distribution
 * install profile (http://drupal.org/project/l10n_install). The timezone
 * names listing was based in the Open Atrium distribution
 * (http://drupal.org/project/openatrium) install profile.
 */

// Prevents an installer timeout when possible. Infinite time execution.
set_time_limit(0);

// Set a cookie with a timezone. After the installer install the profile
// modules, it makes a full bootstrap. Since the Date Timezone sets the
// timezone to UTC when the date_timezone_default_name variable is not set,
// we use the initial timezone name stored in a cookie to suggests it to
// the user installing the site.
drupal_add_js(
  array(
    'drupen' => array(
      'timezone' => date_default_timezone_get()
    )
  ),
  'setting'
);

drupal_add_js('profiles/drupen/jquery.cookie.js', 'module');
drupal_add_js('profiles/drupen/drupen.js', 'module');

/**
 * Returns an array of default values for the installer configure site form and
 * others tasks. Available fields are:
 *
 * - admin_name
 * - admin_mail
 * - admin_theme
 * - site_name
 * - site_mail
 * - theme_default
 *
 * @return
 *   An array of default values for the installer.
 */
function drupen_site_information() {
  return array();
}

/**
 * Implementation of hook_profile_details().
 */
function drupen_profile_details() {
  return array(
    'name'        => 'Drüpen',
    'description' => 'Select this profile to install the Drüpen distribution.',
  );
}

/**
 * Implementation of hook_profile_modules().
 */
function drupen_profile_modules() {
  $core    = _drupen_namespace_modules('core');
  $contrib = _drupen_namespace_modules('contrib');

  return array_merge($core, $contrib);
}

/**
 * Implementation of hook_profile_task_list().
 */
function drupen_profile_task_list() {
  $tasks = array();

  // Strings from l10n_update.
  st('Updating translation.');
  st('Updating translations.');
  st('Downloading and importing files.');
  st('Error importing interface translations');
  st('Importing downloaded translation: %url.');
  st('Failed download from %url');

  if (!_drupen_default_language()) {
    $tasks['import-translations-batch'] = st('Import translations');
  } else {
    $tasks['import-translations-batch'] = st('Setup site');
  }

  return $tasks + array(
    'install-extra-modules-batch'    => st('Install additional modules'),
    'install-features-modules-batch' => st('Install Features modules'),
    'revert-features-batch'          => st('Revert Features modules'),
  );
}

/**
 * Implementation of hook_profile_tasks().
 */
function drupen_profile_tasks(&$task, $url) {
  $output = '';

  if ($task == 'profile') {
    if (!_drupen_default_language()) {
      _drupen_import_translations($url);
    } else {
      $task = 'site-setup';
    }
  }

  if ($task == 'site-setup') {
    _drupen_site_setup();
    $task = 'install-extra-modules';
  }

  if ($task == 'install-extra-modules') {
    _drupen_install_extra_modules($url);
  }

  if ($task == 'install-features-modules') {
    _drupen_install_features_modules($url);
  }

  if ($task == 'revert-features') {
    _drupen_revert_features($url);
  }

  // We are running a batch task for this profile so basically do nothing and
  // return page.
  if (in_array($task, array_keys(drupen_profile_task_list()))) {
    include_once 'includes/batch.inc';
    $output = _batch_page();
  }

  return $output;
}

/**
 * Defines core modules to be installed. Exposes hook_profile_core_modules().
 *
 * @return
 *  An array of Drupal core modules to install.
 */
function drupen_profile_core_modules() {
  // Path and Search are not installed in the Drupal default profile. Color
  // module (available in Drupal default) was excluded here.
  return array('comment', 'help', 'menu', 'taxonomy', 'dblog', 'path', 'search');
}

/**
 * Defines contrib modules to be installed. Exposes
 * hook_profile_contrib_modules().
 *
 * When possible, list contrib module in hook_profile_extra_modules() and
 * try to list the minimum possible modules to install in this hook to prevent
 * a timeout of the installer.
 *
 * @return
 *   An array of Drupal contrib modules to install.
 */
function drupen_profile_contrib_modules() {
  $modules = array(
    // Exportables
    'ctools', 'features',

    // Context
    'context',

    // CCK
    'content', 'text', 'optionwidgets', 'nodereference', 'fieldgroup',

    // CCK contrib
    'filefield', 'filefield_meta', 'imagefield', 'imagefield_crop', 'nodereference_url',

    // Date
    'date_api', 'date_timezone',

    // Image
    'imageapi', 'imageapi_gd', 'imagecache',

    // jQuery
    'jquery_update', 'jquery_ui',

    // Utilities
    'autoload', 'libraries', 'token', 'transliteration',

    // Views
    'views', 'views_bulk_operations', 'actions_permissions',
  );

  if (isset($_SERVER['SERVER_SOFTWARE'])) {
    $server = strtolower($_SERVER['SERVER_SOFTWARE']);

    // Only install the Global Redirect module if server is not Microsoft IIS.
    // There is a problem with clean urls and ISAPI, let's Windows users enable
    // the module in post-installation.
    // WIP issue: http://drupal.org/node/166095
    if (FALSE === strpos($server, 'iis')) {
      $modules[] = 'globalredirect';
    }

    // Only install the Private Download module if server is Apache, since it
    // relies on URL rewriting through .htaccess. Note that subdirectories are
    // not private: http://drupal.org/node/1181832
    if (FALSE !== strpos($server, 'apache')) {
      $modules[] = 'private_download';
    }
  }

  // CKEditor Link only install if Locale is available. Also, install the
  // Localization update module. If a user enables a new language, it will
  // automatically fetch data from localize.drupal.org or translate.drupen.org.
  $modules[] = 'locale';
  $modules[] = 'l10n_update';
  $modules[] = 'drupen_update';

  return $modules;
}

/**
 * Defines extras modules to be installed. Exposes
 * hook_profile_extra_modules().  The modules are installed in a separated
 * installer task than the default hook_profile_modules(). To prevent errors,
 * add first a module that is a dependency of a module you want to install
 * (if not already defined in hook_profile_core() or hook_profile_contrib()
 * hooks).
 *
 * Example: if a module, like Date Locale must be installed, list the Date
 * module before because it's a mandatory dependency.
 *
 * @return
 *  An array of modules that to install.
 */
function drupen_profile_extra_modules() {
  return array(
    // Content profile
    'content_profile',

    // Content management tools
    'ckeditor', 'ckeditor_swf', 'ckeditor_link',
    'imce', 'imce_mkdir',
    'private', 'scheduler',

    // Date
    'date', 'date_locale', 'date_popup', 'date_tools',

    // Input formats
    'htmlpurifier', 'authoring_html',

    // Search
    'search404', 'search_config',

    // SEO
    'googleanalytics', 'seochecklist',
    'nodewords', 'nodewords_admin', 'nodewords_basic', 'nodewords_custom_pages', 'nodewords_extra', 'nodewords_ui', 'nodewords_verification_tags',
    'xmlsitemap', 'xmlsitemap_engines', 'xmlsitemap_menu', 'xmlsitemap_node', 'xmlsitemap_taxonomy',

    // Social networking & Content promotion
    'comment_notify', 'gravatar', 'service_links', 'general_services', 'widget_services',

    // UI
    'admin', 'better_formats', 'date_popup_authored', 'diff', 'pagepreview',
    'nodeformcols', 'vertical_tabs',

    // User management
    'adminrole',

    // Utilities
    'getid3',

    // Views
    'views_attach', 'views_rss',
  );
}

/**
 * Defines Features modules to be installed. Exposes
 * hook_profile_features_modules(). The modules are installed in a separated
 * installer task than the default hook_profile_modules().
 *
 * @return
 *  An array of Features modules to install as keys, with an array of
 *  components to be reverted. features => array(components).
 */
function drupen_profile_features_modules() {
  return array(
    'drupen'         => array(),
    'drupen_admin'   => array(),
    'drupen_profile' => array('content'), // drupen_blog depends of it
    'drupen_blog'    => array(),
    'drupen_gallery' => array(),
    'drupen_news'    => array(),
    'drupen_theming' => array(),
  );
}

/**
 * Defines modules that should not be installed. It can be any module type
 * defined in other hooks. This installer filters all the other hooks before
 * giving the modules list to the installer process.
 *
 * @return
 *   An array of modules to not install.
 */
function drupen_profile_blacklist_modules() {
  return array();
}

/**
 * Install task. Creates the node type page (like the Drupal default profile)
 * and set the admin and default themes.
 */
function _drupen_site_setup() {
  // Creates the page type. This is identical to the page node type created in
  // the Drupal default profile.
  $page_type = array(
    'type'           => 'page',
    'name'           => st('Page'),
    'module'         => 'node',
    'description'    => st("A <em>page</em> is a simple method for creating and displaying information that rarely changes, such as an \"About us\" section of a website. By default, a <em>page</em> entry does not allow visitor comments."),
    'custom'         => TRUE,
    'modified'       => TRUE,
    'locked'         => FALSE,
    'help'           => '',
    'min_word_count' => '',
  );

  $page_type = (object) _node_type_set_defaults($page_type);
  node_type_save($page_type);

  // Update the menu router information.
  menu_rebuild();

  $config = _drupen_site_information();

  // Themes. Defaults: admin uses "Rubik" and the site uses "October".
  $admin_theme = isset($config['admin_theme']) ? $config['admin_theme'] : 'rubik';
  $theme_default = isset($config['theme_default']) ? $config['theme_default'] : 'october';

  variable_set('admin_theme', $admin_theme);
  variable_set('theme_default', $theme_default);

  // Enable themes.
  $theme_sql = "UPDATE {system} SET status = 1 WHERE name = '%s' AND type = '%s'";
  db_query($theme_sql, 'rubik', 'theme');
  db_query($theme_sql, 'zen', 'theme');
  db_query($theme_sql, $config['admin_theme'], 'theme');
  db_query($theme_sql, $config['theme_default'], 'theme');

  // Uses admin theme to edit nodes.
  variable_set('node_admin_theme', 1);

  // New user registration requires admin approval.
  variable_set('user_email_verification', 1);
  variable_set('user_register', 2);
  variable_set('user_registration_help', st('Your account will be queued for approval. Once approved, you will receive an email with an address where you will be able to set your password.'));
}

/**
 * Install task. Creates a batch process to import all available translations
 * for all available modules and themes.
 *
 * @param $url
 *  The current installer URL.
 */
function _drupen_import_translations($url) {
  // Import translations for disabled modules/themes too.
  variable_set('l10n_update_check_disabled', 1);

  // Only import translations from remote server.
  variable_set('l10n_update_check_mode', L10N_UPDATE_CHECK_REMOTE);

  $history = l10n_update_get_history();
  module_load_include('check.inc', 'l10n_update');
  $available = l10n_update_available_releases();
  $updates = l10n_update_build_updates($history, $available);

  module_load_include('batch.inc', 'l10n_update');
  $updates = _l10n_update_prepare_updates($updates, NULL, array());
  $batch = l10n_update_batch_multiple($updates, LOCALE_IMPORT_KEEP);

  // Overwrite batch finish callback.
  $batch['finished'] = '_drupen_import_translations_finished';

  variable_set('install_task', 'import-translations-batch');
  batch_set($batch);
  batch_process($url, $url);
}

/**
 * Callback for the import-translations-batch task.
 */
function _drupen_import_translations_finished($success, $results) {
  if ($success) {
    // Set the next task.
    variable_set('install_task', 'site-setup');

    // Only checks translations for enabled modules/themes.
    variable_set('l10n_update_check_disabled', 0);

    // Check daily for new translations.
    variable_set('l10n_update_check_frequency', 1);

    // Import translations from local and remote sources.
    variable_set('l10n_update_check_mode', L10N_UPDATE_CHECK_ALL);

    // Import new translations. Does not update old ones.
    variable_set('l10n_update_import_mode', LOCALE_IMPORT_KEEP);
  }

  // Invoke default batch finish function too.
  module_load_include('batch.inc', 'l10n_update');
  _l10n_update_batch_finished($success, $results);
}

/**
 * Install task. Creates a batch process to install the extra modules.
 *
 * @param $url
 *  The current installer URL.
 */
function _drupen_install_extra_modules($url) {
  $modules = _drupen_namespace_modules('extra');

  $batch = array(
    'finished'      => '_drupen_install_extra_modules_finished',
    'title'         => st('Installing additional modules'),
    'error_message' => st('The installation has encountered an error.')
  );

  _drupen_install_modules(
    $modules,
    'install-extra-modules-batch',
    $batch,
    $url
  );
}

/**
 * Callback for the install-extra-modules-batch task.
 */
function _drupen_install_extra_modules_finished() {
  variable_set('install_task', 'install-features-modules');
}

/**
 * Install task. Creates a batch process to install the Features modules.
 *
 * @param $url
 *  The current installer URL.
 */
function _drupen_install_features_modules($url) {
  $modules = _drupen_namespace_modules('features');

  $batch = array(
    'finished'      => '_drupen_install_features_modules_finished',
    'title'         => st('Installing Features modules'),
    'error_message' => st('The installation has encountered an error.')
  );

  _drupen_install_modules(
    array_keys($modules),
    'install-features-modules-batch',
    $batch,
    $url
  );
}

/**
 * Callback for the install-features-modules-batch task.
 */
function _drupen_install_features_modules_finished() {
  variable_set('install_task', 'revert-features');
}

/**
 * Install task. Creates a batch process to revert the Features modules.
 *
 * @param $url
 *  The current installer URL.
 */
function _drupen_revert_features($url) {
  $operations = array();

  // Flush Drupal cache.
  $operations[] = array('drupal_flush_all_caches', array());

  $files = module_rebuild_cache();
  $modules = _drupen_namespace_modules('features');

  // Add each feature module to be reverted in a separated batch operation.
  foreach ($modules as $module => $components) {
    $operations[] = array(
      'features_revert',
      array(
        array($module => $components),
        $files[$module]->info['name']
      )
    );
  }

  $batch = array(
    'operations'    => $operations,
    'finished'      => '_drupen_revert_features_finished',
    'title'         => st('Reverting Features modules components'),
    'error_message' => st('The installation has encountered an error.'),
  );

  variable_set('install_task', 'revert-features-batch');
  batch_set($batch);
  batch_process($url, $url);
}

/**
 * Callback for the revert-features-batch task.
 */
function _drupen_revert_features_finished() {
  variable_set('install_task', 'profile-finished');
}

/**
 * Creates a batch process to install modules.
 *
 * @param $modules
 *   An array of modules to be installed.
 *
 * @param $task_name
 *   The name of the batch task to execute. The installer reload will use it
 *   to render the batch progress page.
 *
 * @param $batch_def
 *   An array with the keys 'finished', 'title' and 'error_message' to configure
 *   the batch process.
 *
 * @param $url
 *   The current installer URL.
 *
 * @param $operations
 *   An array of operations to execute before the installation of the modules.
 */
function _drupen_install_modules(array $modules, $task_name, array $batch_def, $url, $operations = array()) {
  $files = module_rebuild_cache();

  foreach ($modules as $module) {
    $operations[] = array(
      '_install_module_batch', array($module, $files[$module]->info['name'])
    );
  }

  $batch = array(
    'operations'    => $operations,
    'finished'      => $batch_def['finished'],
    'title'         => $batch_def['title'],
    'error_message' => $batch_def['error_message']
  );

  variable_set('install_task', $task_name);
  batch_set($batch);
  batch_process($url, $url);
}

/**
 * Returns TRUE if the install language is English.
 *
 * @return
 *   Boolean. TRUE if the install language is English (default locale).
 */
function _drupen_default_language() {
  global $install_locale;

  return empty($install_locale) || 'en' === $install_locale;
}

/**
 * Returns the name of the install profile being installed.
 *
 * @return
 *   The install profile name being installed.
 */
function _drupen_active_profile() {
  $profile = NULL;

  if (isset($_GET['profile']) && 'drupen' != $_GET['profile']) {
    $profile = $_GET['profile'];
  }

  return $profile;
}

/**
 * This function enables the hook_site_information() settings overriding.
 *
 * @return
 *  An array of default values for the installer.
 */
function _drupen_site_information() {
  $defaults = array(
    'admin_name'      => 'admin',
    'admin_mail'      => 'admin@example.org',
    'admin_theme'     => 'rubik',
    'install_profile' => 'drupen',
    'site_name'       => 'Drüpen',
    'site_mail'       => 'site@example.org',
    'theme_default'   => 'october'
  );

  $override = _drupen_profile_hook_invoke('site_information');
  if (NULL !== $override) {
    return array_merge($defaults, $override);
  }

  return $defaults;
}

/**
 * Calls a function, passing extra arguments as the function argument.
 *
 * @param $args...
 *   The function to call. Extra arguments as the function argument.
 *
 * @return
 *   The return of the called function.
 */
function _drupen_profile_hook_invoke() {
  $profile_name = _drupen_active_profile();
  if (is_null($profile_name)) {
    return NULL;
  }

  $args = func_get_args();
  $hook = $profile_name . '_' . $args[0];
  if (! function_exists($hook)) {
    return NULL;
  }

  unset($args[0]);
  return call_user_func_array($hook, $args);
}

/**
 * Returns modules for a namespace. A namespace can be one of:
 *
 * - core
 * - contrib
 * - extra
 * - features
 * - blacklist
 *
 * Modules defined in hook_profile_blacklist_modules() are not returned if
 * the passed namespace is not 'blacklist'.
 *
 * @param $namespace
 *   The namespace to retrieve modules for.
 *
 * @return
 *   An array of the filtered modules.
 */
function _drupen_namespace_modules($namespace) {
  $hook_name = sprintf('profile_%s_modules', $namespace);

  $modules = array();
  $profile_modules = _drupen_profile_hook_invoke($hook_name);
  if (! is_null($profile_modules)) {
    $modules = $profile_modules;
  }

  $hook = 'drupen_' . $hook_name;
  $modules = array_merge($hook(), $modules);
  $modules = _drupen_filter_namespace_modules($namespace, $modules);

  return $modules;
}

/**
 * Filters an array of modules for a namespace, removing modules defined
 * in the hook_profile_blacklist_modules().
 *
 * @param $namespace
 *   The $modules namespace.
 *
 * @param $modules
 *   An array of modules names for the $namespace.
 *
 * @return
 *   An array of filtered modules.
 */
function _drupen_filter_namespace_modules($namespace, array $modules) {
  // Prevents infinite recursion.
  if ($namespace == 'blacklist') {
    return $modules;
  }

  $blacklist = _drupen_namespace_modules('blacklist');

  $filtered_modules = array();
  if ($namespace == 'features') {
    foreach ($modules as $key => $value) {
      if (! in_array($key, $blacklist)) {
        $filtered_modules[$key] = $value;
      }
    }
  }
  else {
    $filtered_modules = array_diff($modules, $blacklist);
  }

  return $filtered_modules;
}

/**
 * Set "Drüpen" as default install profile.
 *
 * Implementation of hook_form_FORM_ID_alter().
 *
 * Since FAPI only call hook_form_alter() implemented by modules (by calling
 * drupal_prepare_form() [form.inc] -> drupal_alter() [common.inc]->
 * module_implements() [module.inc]). We use the system module namespace because
 * it's a core module available early in install_main().
 *
 * @link http://api.drupal.org/api/function/hook_form_FORM_ID_alter/6
 */
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  $config = _drupen_site_information();

  foreach($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = $config['install_profile'];
  }
}

/**
 * Set default values for the site configure form.
 *
 * Implementation of hook_form_FORM_ID_alter().
 */
function system_form_install_configure_form_alter(&$form, $form_state) {
  $config = _drupen_site_information();

  $form['site_information']['site_name']['#default_value']    = $config['site_name'];
  $form['site_information']['site_mail']['#default_value']    = $config['site_mail'];
  $form['admin_account']['account']['name']['#default_value'] = $config['admin_name'];
  $form['admin_account']['account']['mail']['#default_value'] = $config['admin_mail'];

  // The 'date_timezone_update_site' validate callback function will update
  // the date_default_timezone variable too, with the calculated offset.
  if (function_exists('date_timezone_names') && function_exists('date_timezone_update_site')) {
    $form['server_settings']['date_default_timezone']['#access'] = FALSE;
    $form['server_settings']['#element_validate'] = array('date_timezone_update_site');
    $form['server_settings']['date_default_timezone_name'] = array(
      '#type'          => 'select',
      '#title'         => st('Site time zone'),
      '#default_value' => '',
      '#options'       => date_timezone_names(FALSE, TRUE),
      '#description'   => st('Select the default site time zone. If in doubt, choose the timezone that is closest to your location which has the same rules for daylight saving time.'),
      '#required'      => TRUE,
    );
  }
}
