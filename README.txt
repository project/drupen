README
==

Introduction
--

Drüpen is a feature rich Drupal distribution aimed to simplify web content
publishing. It comes with:

 - Text editor: it comes with the fully featured CKEditor, with a fine tuned
   toolbar to help content editors with text and media publishing.

 - Improved media support: content editors can embed iframe video players from
   video sites like YouTube and Vimeo, securely. If a mp3 file is uploaded, an
   audio player will automatically be loaded, where the site audience will can
   listen the audio without downloading it.

 - Fully SEO featured. Simplifies tasks like Google Analytics tracking code
   setup, Google/Bing Webmaster Tools setup (via meta tags) and content meta
   tags.

 - Improved admin UI and extra user roles.

Visit http://drupen.org for more information.

The Drüpen project is sponsored by Infranology (http://infranology.com).


Requirements
--

Drüpen was tested with the following software successfully.

 - PHP 5.2+
 - PHP GD, XML and XMLWriter extensions
 - 64MB memory limit
  - 64bit environment may need more memory, specially to enable more modules
 - MySQL 5
 - Apache
 - Apache mod_rewrite for Drupal clean URLs support
 - Internet connection, the installer will automatically download and import
   translations if installing in a language other than English


Installing
--

Download a fully packaged release from http://drupen.org. The official releases
are packaged with the supported languages of Drüpen. The installation steps of
Drüpen are the same for the Drupal. In simple steps (Linux, command line):

 1. Download and unpack Drüpen:

    $ tar xzvf drupen-6.x-1.0.tgz

 2. Move to the web server root:

    $ mv drupen /var/www

 3. Adjust permissions for the `sites/default/settings.php` file and
    `sites/default/files` directory:

    $ cd /var/www/drupen
    $ chmod 775 sites/default/settings.php sites/default/files
    $ chgrp <THE-WEBSERVER-USER> sites/default/settings.php sites/default/files

 4. Create the database:

    $ mysql -u user -p
    $ CREATE DATABASE drupen;

 5. In your browser, visit http://localhost/drupen and proceed with the guided
    installation.

 6. After a successfully installation, restore the `sites/default/settings.php`
    file permissions:

    $ chmod 644 sites/default/settings.php

Visit http://drupen.org/documentation for more information.


Special thanks
--

To the great Drupal community and to the open source world.
